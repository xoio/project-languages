/// <reference types="vite/client" />

interface LangData {
    name: string,
    pct: number
}

interface ProjectData {
    id: number
    name: string
    repo_url: string
    languages?: Array<LangData>
    type: number
}
