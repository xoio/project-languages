export const API_BASE = "https://gitlab.com/api/v4"

export function getCacheKey(usernanme: string) {
    return `gitlab-${usernanme}`
}

export function getCacheDate(username: string) {
    return `gitlab-${username}-cache-date`
}

/**
 * Returns a list of the languages used in the project
 * @param proj {number} the project id
 */
export function get_project_languages_url(proj: number) {
    return `${API_BASE}/projects/${proj}/languages`
}

/**
 * Returns a list of projects for the specified user/group
 * @param user {string} the user/group name to find
 * @param isGroup {boolean} whether or not the username is a group name
 * @param numProjects {number} maximum number of projects to return
 */
export function get_projects_url(user: string, isGroup: boolean = false, numProjects = 100) {
    if (isGroup) {
        return `${API_BASE}/groups/${user}/projects?per_page=${numProjects}`
    } else {
        return `${API_BASE}/users/${user}/projects?per_page=${numProjects}`
    }
}

export enum REPO_TYPES {
    PERSONAL,
    GROUP
}
