const state = $state({
    currentLanguageFilter: null,
    cacheDate: "",

    // the available languages to select from
    languages: [""],

    // the currently selected languages
    selectedLanguages: [],

    updateLanguages(langs: string[]) {
        this.languages = langs
    },
    getLanguages() {
        return this.languages
    }

})

export default state
