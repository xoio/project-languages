import {get_project_languages_url, get_projects_url, REPO_TYPES} from "./constants.ts";
import {ProjectData} from "./vite-env";

export async function pullProjects() {
    let personal = []
    let group = []

    {

        // pull personal projects
        personal = await fetch(get_projects_url("sortofsleepy")).then(res => res.json())
        personal = personal.map((itm: any) => {
            let d: ProjectData = {
                id: itm.id,
                name: itm.name,
                repo_url: itm.http_url_to_repo,
                languages: [],
                type: REPO_TYPES.PERSONAL

            }
            return d
        })
        let lang_data = personal.map(async (proj: ProjectData) => {
            return await fetch(get_project_languages_url(proj.id)).then(res => res.json())
        })
        lang_data = await Promise.all(lang_data)
        lang_data.forEach((data: any, idx: number) => {
            for (let i in data) {
                personal[idx].languages.push({
                    name: i,
                    pct: data[i]
                })
            }
        })

    }
    ////////////////////////////////

    // pull group projects

    {
        group = await fetch(get_projects_url("xoio", true)).then(res => res.json())
        group = group.map((itm: any) => {
            let d: ProjectData = {
                id: itm.id,
                name: itm.name,
                repo_url: itm.http_url_to_repo,
                languages: [],
                type: REPO_TYPES.GROUP
            }
            return d
        })

        let lang_data = group.map(async (proj: ProjectData) => {
            return await fetch(get_project_languages_url(proj.id)).then(res => res.json())
        })
        lang_data = await Promise.all(lang_data)
        lang_data.forEach((data: any, idx: number) => {
            for (let i in data) {
                group[idx].languages.push({
                    name: i,
                    pct: data[i]
                })
            }
        })
    }

    return {
        personal,
        group
    }
}
