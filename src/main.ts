import './style.pcss'

import App from "./app.svelte"
import {mount} from "svelte";

mount(App, {
    target: document.querySelector("#app") as HTMLElement
})
